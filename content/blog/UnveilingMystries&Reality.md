---
title: Unveiling Mysteries & Reality
date: 2023-07-02
description: Trial.
tag: life
---


![](/img/blog/a54bbf57.webp)


Poetic adventures are coming to an end. I am still on the path of self discovery. Although failier seems to outrun success most of the time. Journey hasn't been all that bad. Sometimes my eyes hurt so badly that I shut them off completely. That's where I find peace. The only thing I wanted on this damn planet. Nothing less nothing more but you see peace is a heavy word, especially in these modern times. Step after step, Sometimes I circle back to reality. A reality that is so much so different from the actual one. Which is filled with mysteries of its own and the war that's been going on inside my head never seems to stop. The illusion of time makes this all go faster than my eyes. In all of this shitstorm, There were surprises I like to forget. Moments of solitude that filled the void for a second. Unlearning myself was a bad idea I ended up learning more. Which I'm not entirely happy about. When you're getting there you're actually not there. Can't remember the last time I wrote something on a cold rainy night like this. The heat waves have been killing me for the last few months but not so much so than 'you'. Which I have forgotten thanks to another certain 'you'. Like everything else this might not make any sense at all. It's just that every day was just another yesterday. Every attempt was just an attempt. I'm kinda tired right now but I can't let peace be at peace. Last I remember the sound of your breathing seems to moan louder. All the protocols I broke & hacked were just simpler. Just because I'm not myself doesn't mean I'm yours. So let's talk back when I concur my mind in the midst of this chaos.
