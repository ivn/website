---
title: You & You 
date: 2023-04-04
description: The Way I See You.
tag: life
---

![](/img/blog/k84sq5.webp)

It's always been you. The one that i don't see, actually i can't see. The one that doesn't appear on my dreams. The one that only show up when i'm my worst. It's you. Remember the times we used to laugh at each other ? Remember the times we used to look at each others eyes ? I do, it utterly pains me to remember those things. All the places you're been to, All the places you're goint to, All the places that you will never go back to. I'm there. In the wind, Waiting ! or maybe not ? The only close step I can't seem to take. Foolish desires & unsaid formulas. Thank you for not saving me from who I am. Maybe that's why you're invinsible. I mean the real you. The one that's hiding. The real owner of your soul. Again it's only you, I've been you, from the inside. Your words that made me high, Your thoughts that made me cry I don't think they'll ever be back. It's time to say 'Good Bye !', doesn't it ? It's only impossible even i was You.
