document.addEventListener("DOMContentLoaded", async () => {
    const imageContainer = document.getElementById("container-memes");
    try {
        const response = await fetch("../json/memes.json");
        const fileNames = await response.json();
        const suffledfileNames = fileNames.sort(() => Math.random() - 0.5);  
        suffledfileNames.forEach(fileName => {
          const img = document.createElement("img");
          img.className = "meme";
          img.setAttribute("loading", "lazy");
          img.src = fileName;
          img.alt = "some meme image probably with some pretty girl";
          imageContainer.appendChild(img);
        });
    } catch (error) {
        console.error("Error fetching file list:", error);
    }
});
